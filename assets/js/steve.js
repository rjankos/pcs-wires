
$( document ).ready(function() {
    //console.log( "ready!" );
    
  
    
    

	// JS for landing-typeXXX pages to show/hide the stage date box content
	$('#noStagingDateMsg').toggle();
	$('#btn1').on('click', function(){
		$('#stagingDateMsg').slideToggle(200);
		$('#noStagingDateMsg').slideToggle(200);
		$('#btn1 .panel-title').html('<i class="fa fa-calendar"></i> Find your staging date');
		$('.advice').slideToggle(200); // show / hide suggest start dates in step panels
	});


	// Staging date triage form stuff (show/hide staging date)
	$('.staging-date-panel').toggle();
	$('#btn-submit-staging-info').on('click', function(){
		$('.form-staging-date').slideToggle(300);
		$('.staging-date-panel').slideToggle(300);

		$('.staging-date').removeClass('hide');// toggling visibility of Staging Date in Duties Tool to give impression this has just been calculated

	});

	// Landing type (employer buckets) make whole CTA panel whole DIV links
	$(document).delegate(".cta", "click", function() {
		// alert('DIV clicked');
		window.location = $(this).find("a").attr("href");
	});


	// step 2 (Choose pension) show / hide info panels (info-panel)
	$('.info-panel .info-panel-body').toggle(); // close info-panel-boy on page load
	$('.info-panel').on('click', function(){
		$(this).find('.info-panel-body').toggle('slide', { direction: 'up', mode: 'show' }, 500);
		$(this).find('.fa').toggleClass('fa-flip-vertical'); //rotates font awesome icon
	});

	// assess staff table links that reveal staff-type-xxx


	// On "Work out who you need to put into a pension scheme" clicking the table label reveals the relevant content in the panel-info below.
	$('.toggle-type-one').on('click', function(){
		$('#staff-type-one').find('.info-panel-body').toggle('slide',{direction:'up',mode:'show'},500);
	});
	$('.toggle-type-two').on('click', function(){
		$('#staff-type-two').find('.info-panel-body').toggle('slide',{direction:'up', mode: 'show'},500);
	});
	$('.toggle-type-three').on('click', function(){
		$('#staff-type-three').find('.info-panel-body').toggle('slide',{direction:'up',mode:'show'},500);
	});


	//homepage alread-links hide / show stuff

	$('.already-links-content').toggle();
	$('.already-links-reveal').on('click', function(){
		// alert('DIV clicked');
		$('.already-links-content').slideToggle(300);
		$(this).find('.fa').toggleClass('fa-flip-vertical'); //rotates font awesome external icon
	});

	// TOM D TOGGLE HEADER INFO ON STATEMENTS DETAIL

	$('a.drugsButton').click(function(){
		$('.headerInfo').toggle();
	});

	// TOM D GRAPH STUFF FROM CHARTIST - http://gionkunz.github.io/chartist-js/

	/* Add a basic data series with six labels and values */

	new Chartist.Line('.ct-chart', {
		labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
		series: [
			[
				102030.96,
				104934.92,
				109091.63,
				204934.92,
				204934.92,
				203091.63,
				304934.92,
				403091.63,
				304934.92,
				203091.63,
				104934.92,
				103091.63
			]
		]
	}, {
		low: 0,
		showArea: true
	});

});


$('.patientSubmit').click(function(){
$('.patientRecords').toggle();
});
