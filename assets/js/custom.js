$(document).ready(function(){
    $(function() {
        var availableTags = [
            "Appleby",
            "Benson",
            "Jones",
            "James",
            "Junquier",
            "Johnson",
            "Jojnston",
            "Cartwright",
            "Davis",
            "00123",
            "00189",
            "00370",
            "00999"
        ];
        $('#patient,#cytology').autocomplete({
            source: availableTags
        });
    });

    $('.patientSubmit').click(function(){
        $('.patientRecords').show();
        $('.cytologyRecords').hide();
    });

    $('.cytologySubmit').click(function(){
        $('.cytologyRecords').show();
        $('.patientRecords').hide();
    });

    $('a.drugsButton').click(function(){
        $('.headerInfo').toggle();
    });

});